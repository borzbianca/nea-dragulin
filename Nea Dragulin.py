"""Nea Drăgulin are un număr natural n pe care îl scrie de k ori, unul după altul.
Aflați restul împărțirii numărului astfel obținut la 72."""

n = input().split()
k = input().split()
nr_repetat = str(n) * int(k)
rest = int(nr_repetat) % 72
print(rest)
